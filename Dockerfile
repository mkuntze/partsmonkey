FROM rust:latest as partsmonkey-backend
WORKDIR /usr/src/partsmonkey/backend
RUN apt-get update && apt-get install -y libpq-dev
RUN cargo install diesel_cli --no-default-features --features postgres
COPY ./backend .
RUN cargo install --path .
EXPOSE 8000
CMD ["bash", "scripts/init.sh"]

FROM node:latest as frontend-builder
WORKDIR /usr/src/partsmonkey/frontend
COPY ./frontend .
RUN npm install
RUN npm run build

FROM nginx:latest as partsmonkey-frontend
COPY --from=frontend-builder /usr/src/partsmonkey/frontend/public /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel;

use std::borrow::Cow;
use std::ops::{Add, Deref};
use std::sync::{Arc, RwLock};
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use jsonwebtoken::{DecodingKey, encode, EncodingKey};
use rand::{RngCore, SeedableRng};
use rocket::data::{FromData, Outcome};
use rocket::{Data, Request, State};
use rocket::form::error::ErrorKind::Validation;
use rocket::http::{Cookie, CookieJar, Status};
use rocket::request::FromRequest;
use rocket::response::status;
use serde::{Serialize, Deserialize};
use rocket_sync_db_pools::{database};
use diesel::prelude::*;
use diesel::pg::PgConnection;

use argon2::{
    password_hash::{
        rand_core::OsRng,
        PasswordHash, PasswordHasher, PasswordVerifier, SaltString
    },
    Argon2
};
use base64::decode;
use rocket::form::Form;

use self::models::*;
use self::schema::*;

pub mod models;
pub mod schema;

#[database("partsmonkey")]
struct PartsmonkeyDbConn(diesel::PgConnection);

struct JWTSecret {
    encoding_key: Arc<RwLock<EncodingKey>>,
    decoding_key: Arc<RwLock<DecodingKey>>
}

#[derive(FromForm)]
struct AuthData {
    email: String,
    password: String
}

#[derive(Debug, Serialize, Deserialize)]
struct AuthTokenClaims {
    sub: i32,
    exp: usize,
    admin: bool
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for User {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> rocket::request::Outcome<Self, Self::Error> {
        if let Some(auth_token) = request.cookies().get("AuthToken") {
            let jwt_key = request.guard::<&State<JWTSecret>>().await.unwrap();
            let conn = request.guard::<PartsmonkeyDbConn>().await.unwrap();

            let claims = jsonwebtoken::decode::<AuthTokenClaims>(auth_token.value(),
                                                                 &jwt_key.decoding_key.read().unwrap(),
                                                                 &jsonwebtoken::Validation::default());
            println!("Received JWT with claims: {:?}", claims);

            if let Ok(claims) = claims {
                if let Some(user) = conn.run(move |c| users::table
                    .filter(users::id.eq(claims.claims.sub)).load(c)).await
                    .expect("Error loading user from DB")
                    .into_iter().next() {
                    return rocket::request::Outcome::Success(user);
                }
            }
        }
        rocket::request::Outcome::Failure((Status::new(403), ()))
    }
}

#[post("/auth/login", data = "<login>")]
async fn login(login: Form<AuthData>, cookies: &CookieJar<'_>, jwt_key: &State<JWTSecret>, connection: PartsmonkeyDbConn) -> Status {
    let password = login.password.clone();
    let user = {
        let maybe_user = connection.run(move |c| users::table
            .filter(users::email.eq(&login.email))
            .load::<User>(c)).await
            .expect("Error loading user").into_iter().next();
        if maybe_user.is_none() {
            return Status::Forbidden;
        }

        maybe_user.unwrap()
    };

    let parsed_hash = PasswordHash::new(&user.password).unwrap();

    if Argon2::default().verify_password(password.as_bytes(), &parsed_hash).is_ok() {
        let token = encode(&jsonwebtoken::Header::default(), &AuthTokenClaims {
            sub: user.id,
            exp: SystemTime::now().add(Duration::from_secs(30 * 60)).duration_since(UNIX_EPOCH).unwrap().as_secs() as usize,
            admin: user.admin
        }, &jwt_key.encoding_key.read().unwrap()).unwrap();

        cookies.add(Cookie::build("AuthToken", token).http_only(true).finish());
        return Status::Ok;
    }

    return Status::Forbidden;
}

#[post("/auth/logout")]
fn logout(user: User, cookies: &CookieJar<'_>) {
    cookies.remove(Cookie::named("AuthToken"));
}

#[post("/auth/regenerate")]
fn regenerate(user: User, cookies: &CookieJar<'_>, jwt_key: &State<JWTSecret>) {
    let jwt_cookie = cookies.get("AuthToken").unwrap();
    let mut claims: AuthTokenClaims = jsonwebtoken::decode(
        jwt_cookie.value(),
        &jwt_key.decoding_key.read().unwrap(),
        &jsonwebtoken::Validation::default())
        .unwrap()
        .claims;

    claims.exp = SystemTime::now().add(Duration::from_secs(30 * 60)).duration_since(UNIX_EPOCH).unwrap().as_secs() as usize;

    let jwt = jsonwebtoken::encode(&jsonwebtoken::Header::default(), &claims, &jwt_key.encoding_key.read().unwrap()).unwrap();

    cookies.add(Cookie::build("AuthToken", jwt).http_only(true).finish());
}

fn generate_jwt_secret() -> JWTSecret {
    let mut prng = rand::rngs::StdRng::from_entropy();
    let mut secret_bytes = vec![0u8; 100];
    prng.fill_bytes(&mut secret_bytes);
    let jwt_secret = base64::encode(&secret_bytes);
    println!("JWT Key: {}", jwt_secret);
    JWTSecret {
        encoding_key: Arc::new(RwLock::new(EncodingKey::from_base64_secret(&jwt_secret).unwrap())),
        decoding_key: Arc::new(RwLock::new(DecodingKey::from_base64_secret(&jwt_secret).unwrap()))
    }
}

#[launch]
fn rocket() -> _ {
    let jwt_secret = generate_jwt_secret();
    rocket::build()
        .attach(PartsmonkeyDbConn::fairing())
        .manage(jwt_secret)
        .mount("/", routes![login, logout, regenerate])
}